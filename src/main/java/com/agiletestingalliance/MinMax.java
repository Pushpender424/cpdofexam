package com.agiletestingalliance;

public class MinMax {

    public int checkGreaterNumber(int firstNumber, int secondNumber) {
        if (secondNumber > firstNumber) {
            return secondNumber;
	}
        else {
            return firstNumber;
	}
    }

}
